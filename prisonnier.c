#include "prisonnier.h"



/*
 note : le cache du gain par individu est irrealiste, un gain est dependant des effectifs de chaque type de joueurs
 => il faut garder les gains de confrontation sous forme matrice n*n (pour n individus) et ne par garder la somme des gains d'un type de joueur particulier

  @TODO 1 nouveaux joueurs :

ZDStrategies
 @TODO pouvoir définir des seuils de mort non pas en fonction des n plus faibles/forts mais en fonction d'un gain minimal a atteindre
 */

int usage(
        int argc, 
        char** argv, 
        int * tabGain,
        int * nbGenerations,
        int * nbConfrontationAvantEvolution,
        int * nbJoueurParType,
        int * seuilMort,
        int * seuilDulication,
        int * afficherGains,
        int * detailGainEvolution,
        int * detailEffectifEvolution
){
    int i;
    char * s;
    if (argc < 14){
        printf("Usage : %s <description des gains> <nbGenerations> <nbConfrontationAvantEvolution> <nbJoueurParType> <seuilMort> <seuilDulication> [--gains] [--details-gains] [--details-effectifs]\n",  argv[0]);
        printf("Descriptions des gains: Suite de 8 entiers  a b c d e f g h :\n");
        printf("    T: Trahir\n");
        printf("    C: Cooperer\n");
        printf("    J1: joueur 1\n");
        printf("    J2: joueur 2\n");
        printf("\n");
        printf("    gains du joueur 1\n");
        printf("    \\J2!   !   !\n");
        printf("     \\ ! T ! C !\n");
        printf("    J1\\!   !   !\n");
        printf("    ---!---!---!\n");
        printf("     T ! a ! b !\n");
        printf("    ---!---!---!\n");
        printf("     C ! c ! d !\n\n\n");


        printf("    gains du joueur 2\n");
        printf("    \\J2!   !   !\n");
        printf("     \\ ! T ! C !\n");
        printf("    J1\\!   !   !\n");
        printf("    ---!---!---!\n");
        printf("     T ! e ! f !\n");
        printf("    ---!---!---!\n");
        printf("     C ! g ! h !\n\n\n");

        
        return 0;
    }
    
    int tabGainTmp[8];
    
    for (i = 0; i < 8;i++){
        tabGainTmp[i] = atoi(argv[i+1]);
    }
    
    initMoteur(tabGain, tabGainTmp[0], tabGainTmp[1], tabGainTmp[2], tabGainTmp[3], tabGainTmp[4], tabGainTmp[5], tabGainTmp[6], tabGainTmp[7]);
    
    
    *nbGenerations = atoi(argv[9]);
    *nbConfrontationAvantEvolution = atoi(argv[10]);
    *nbJoueurParType = atoi(argv[11]);
    *seuilMort = atoi(argv[12]);
    *seuilDulication = atoi(argv[13]);
    
    *afficherGains=0;
    *detailGainEvolution=0;
    *detailEffectifEvolution=0;
    for(i=14;i<argc;i++){
        s = argv[i];
        if(strcmp(s, "--gains") == 0){
            *afficherGains=1;
        }
        else if(strcmp(s, "--details-gains") == 0){
            *afficherGains=1;
            *detailGainEvolution=1;
        }
        else if(strcmp(s, "--details-effectifs") == 0){
            *detailEffectifEvolution=1;
        }
    }
    
    return 1;
}

int main(int argc, char** argv) {
    int nbConfrontationAvantEvolution;
    int nbGenerations;
    int nbJoueurParType;
    int seuilMort;
    int seuilDulication;
    int detailEffectifEvolution;
    int detailGainEvolution;
    int afficherGains;
    int **tabEffectifEvolution;
    int **tabGainEvolution;
    int i,j,retour;
    
    int  * tabGain;
    tabGain = (int *) malloc (sizeof(int) * 8);
    // 1 5 0 3 1 0 5 3 200 1000 50 1 1
    
    if(usage(
        argc, 
        argv, 
        tabGain,
        &nbGenerations,
        &nbConfrontationAvantEvolution,
        &nbJoueurParType,
        &seuilMort,
        &seuilDulication,
        &afficherGains,
        &detailGainEvolution,
        &detailEffectifEvolution
        
)){
        srand(time(NULL));
        int nbTypeJoueur = NBTYPEJOUEUR;

        if(detailEffectifEvolution == 1){
            tabEffectifEvolution = (int **) malloc(sizeof(int *) * nbGenerations);
            for (i=0;i < nbGenerations;i++){
                tabEffectifEvolution[i] = (int *) malloc(sizeof(int) * nbTypeJoueur);
                for (j=0;j < nbTypeJoueur;j++){
                    tabEffectifEvolution[i][j] = 0;
                }
            }
        }
        else{
            tabEffectifEvolution  = (int **) malloc(sizeof(int *) * 2);
            tabEffectifEvolution[0] = (int *) malloc(sizeof(int) * nbTypeJoueur);
            tabEffectifEvolution[1] = (int *) malloc(sizeof(int) * nbTypeJoueur);
            for (j=0;j < nbTypeJoueur;j++){
                tabEffectifEvolution[0][j] = 0;
                tabEffectifEvolution[1][j] = 0;
            }
        }
        
        ajusterEffectif(nbTypeJoueur, tabEffectifEvolution[0], nbJoueurParType);

        if(detailGainEvolution == 1){
            tabGainEvolution = (int **) malloc(sizeof(int *) * nbGenerations);
            for (i=0;i < nbGenerations;i++){
                tabGainEvolution[i] = (int *) malloc(sizeof(int) * nbTypeJoueur);
            }
        }
        else{
            tabGainEvolution  = (int **) malloc(sizeof(int *));
            tabGainEvolution[0] = (int *) malloc(sizeof(int) * nbTypeJoueur);
        }


        arene(tabGain, nbTypeJoueur, tabEffectifEvolution, detailEffectifEvolution, tabGainEvolution, detailGainEvolution, nbConfrontationAvantEvolution, nbGenerations, seuilMort, seuilDulication);
        afficher_resultats(tabEffectifEvolution, detailEffectifEvolution, tabGainEvolution, detailGainEvolution, afficherGains, nbGenerations, 12, "!");

        if(detailEffectifEvolution == 1){
            for (i=0;i < nbGenerations;i++){
                free(tabEffectifEvolution[i]);
            }
            free(tabEffectifEvolution);
        }
        else{
            free(tabEffectifEvolution[0]);
            free(tabEffectifEvolution[1]);
            free(tabEffectifEvolution);
        }

        if(detailGainEvolution == 1){
            for (i=0;i < nbGenerations;i++){
                free(tabGainEvolution[i]);
            }
            free(tabGainEvolution);
        }
        else{
            free(tabGainEvolution[0]);
            free(tabGainEvolution);
        }
        retour = EXIT_SUCCESS;
    }
    else{
        retour = EXIT_FAILURE;
    }
    
    free(tabGain);
   
    
   
    return retour;
}

