


CC=gcc
CFLAGS=-Wall -ansi -std=c99 
LDFLAGS=
EXEC=prisonnier test


all: $(EXEC)

.PHONY: clean mrproper


test: test.o \
	common/common_empty_functions.o \
	joueurs/JoueurGentil.o \
	joueurs/JoueurMechant.o \
	joueurs/JoueurLunatique.o \
	joueurs/JoueurDonnantDonnant.o \
	joueurs/JoueurMefiant.o \
	joueurs/JoueurDonnantDonnantDur.o  \
	joueurs/JoueurGraduel.o	\
	joueurs/JoueurMajoMou.o	\
	joueurs/JoueurMajoDur.o \
	joueurs/JoueurPavlov.o \
	joueurs/JoueurCCT.o \
	joueurs/JoueurTTC.o \
	joueurs/JoueurRancunier.o \
	joueurs/JoueurSondeur.o \
	joueurs/JoueurDetective.o \
	joueurs/declarerJoueurs.o \
	common/MoteurJeu.o \
	tests/commonTest.o \
	tests/JoueurDonnantDonnant.o \
	tests/JoueurDonnantDonnantDur.o \
	tests/JoueurGentil.o \
	tests/JoueurGraduel.o \
	tests/JoueurLunatique.o \
	tests/JoueurMajoDur.o \
	tests/JoueurMajoMou.o \
	tests/JoueurMechant.o \
	tests/JoueurMefiant.o \
	tests/JoueurPavlov.o \
	tests/JoueurCCT.o \
	tests/JoueurTTC.o \
	tests/JoueurRancunier.o \
	tests/JoueurSondeur.o \
	tests/JoueurDetective.o 
	
	$(CC) -o test \
	    test.o \
	    common/common_empty_functions.o \
	    joueurs/JoueurGentil.o \
	    joueurs/JoueurMechant.o \
	    joueurs/JoueurLunatique.o \
	    joueurs/JoueurDonnantDonnant.o \
	    joueurs/JoueurMefiant.o \
	    joueurs/JoueurDonnantDonnantDur.o \
	    joueurs/JoueurGraduel.o \
	    joueurs/JoueurMajoMou.o \
	    joueurs/JoueurMajoDur.o \
	    joueurs/JoueurPavlov.o \
	    joueurs/JoueurCCT.o \
	    joueurs/JoueurTTC.o \
	    joueurs/JoueurRancunier.o\
	    joueurs/JoueurSondeur.o \
	    joueurs/JoueurDetective.o \
	    joueurs/declarerJoueurs.o \
	    common/MoteurJeu.o \
	    tests/commonTest.o \
	    tests/JoueurDonnantDonnant.o \
	    tests/JoueurDonnantDonnantDur.o \
	    tests/JoueurGentil.o \
	    tests/JoueurGraduel.o \
	    tests/JoueurLunatique.o \
	    tests/JoueurMajoDur.o \
	    tests/JoueurMajoMou.o \
	    tests/JoueurMechant.o \
	    tests/JoueurMefiant.o \
	    tests/JoueurPavlov.o \
	    tests/JoueurCCT.o \
	    tests/JoueurTTC.o \
	    tests/JoueurRancunier.o \
	    tests/JoueurSondeur.o \
	    tests/JoueurDetective.o \
	    $(CFLAGS)

joueurs/JoueurGentil.o: joueurs/JoueurGentil.c 
	$(CC) -c -o joueurs/JoueurGentil.o joueurs/JoueurGentil.c $(CFLAGS)

common/common_empty_functions.o: common/common_empty_functions.c
	$(CC) -c -o common/common_empty_functions.o common/common_empty_functions.c $(CFLAGS)

joueurs/JoueurMechant.o: joueurs/JoueurMechant.c 
	$(CC) -c -o joueurs/JoueurMechant.o joueurs/JoueurMechant.c $(CFLAGS)

joueurs/JoueurMefiant.o: joueurs/JoueurMefiant.c 
	$(CC) -c -o joueurs/JoueurMefiant.o joueurs/JoueurMefiant.c $(CFLAGS)

joueurs/JoueurLunatique.o: joueurs/JoueurLunatique.c 
	$(CC) -c -o joueurs/JoueurLunatique.o joueurs/JoueurLunatique.c $(CFLAGS)

joueurs/JoueurDonnantDonnant.o: joueurs/JoueurDonnantDonnant.c 
	$(CC) -c -o joueurs/JoueurDonnantDonnant.o joueurs/JoueurDonnantDonnant.c $(CFLAGS)

joueurs/JoueurDonnantDonnantDur.o: joueurs/JoueurDonnantDonnantDur.c 
	$(CC) -c -o joueurs/JoueurDonnantDonnantDur.o joueurs/JoueurDonnantDonnantDur.c $(CFLAGS)
	
joueurs/JoueurGraduel.o: joueurs/JoueurGraduel.c 
	$(CC) -c -o joueurs/JoueurGraduel.o joueurs/JoueurGraduel.c $(CFLAGS)
	
 

joueurs/JoueurMajoDur.o: joueurs/JoueurMajoDur.c 
	$(CC) -c -o joueurs/JoueurMajoDur.o joueurs/JoueurMajoDur.c $(CFLAGS)
	
joueurs/JoueurMajoMou.o: joueurs/JoueurMajoMou.c 
	$(CC) -c -o joueurs/JoueurMajoMou.o joueurs/JoueurMajoMou.c $(CFLAGS)

joueurs/JoueurPavlov.o: joueurs/JoueurPavlov.c 
	$(CC) -c -o joueurs/JoueurPavlov.o joueurs/JoueurPavlov.c $(CFLAGS)

joueurs/JoueurCCT.o: joueurs/JoueurCCT.c 
	$(CC) -c -o joueurs/JoueurCCT.o joueurs/JoueurCCT.c $(CFLAGS)

joueurs/JoueurTTC.o: joueurs/JoueurTTC.c 
	$(CC) -c -o joueurs/JoueurTTC.o joueurs/JoueurTTC.c $(CFLAGS)
	
joueurs/JoueurRancunier.o: joueurs/JoueurRancunier.c 
	$(CC) -c -o joueurs/JoueurRancunier.o joueurs/JoueurRancunier.c $(CFLAGS)
	
joueurs/JoueurSondeur.o: joueurs/JoueurSondeur.c 
	$(CC) -c -o joueurs/JoueurSondeur.o joueurs/JoueurSondeur.c $(CFLAGS)

joueurs/JoueurDetective.o: joueurs/JoueurDetective.c 
	$(CC) -c -o joueurs/JoueurDetective.o joueurs/JoueurDetective.c $(CFLAGS)

joueurs/declarerJoueurs.o: joueurs/declarerJoueurs.c
	$(CC) -c -o joueurs/declarerJoueurs.o joueurs/declarerJoueurs.c $(CFLAGS)

common/MoteurJeu.o: common/MoteurJeu.c 
	$(CC) -c -o common/MoteurJeu.o common/MoteurJeu.c $(CFLAGS)
	
tests/commonTest.o: tests/commonTest.c 
	$(CC) -c -o tests/commonTest.o tests/commonTest.c $(CFLAGS)

tests/JoueurDonnantDonnant.o: tests/JoueurDonnantDonnant.c 
	$(CC) -c -o tests/JoueurDonnantDonnant.o tests/JoueurDonnantDonnant.c $(CFLAGS)

tests/JoueurDonnantDonnantDur.o: tests/JoueurDonnantDonnantDur.c 
	$(CC) -c -o tests/JoueurDonnantDonnantDur.o tests/JoueurDonnantDonnantDur.c $(CFLAGS)

tests/JoueurGentil.o: tests/JoueurGentil.c 
	$(CC) -c -o tests/JoueurGentil.o tests/JoueurGentil.c $(CFLAGS)

tests/JoueurGraduel.o: tests/JoueurGraduel.c 
	$(CC) -c -o tests/JoueurGraduel.o tests/JoueurGraduel.c $(CFLAGS)

tests/JoueurLunatique.o: tests/JoueurLunatique.c 
	$(CC) -c -o tests/JoueurLunatique.o tests/JoueurLunatique.c $(CFLAGS)

tests/JoueurMajoDur.o: tests/JoueurMajoDur.c 
	$(CC) -c -o tests/JoueurMajoDur.o tests/JoueurMajoDur.c $(CFLAGS)

tests/JoueurMajoMou.o: tests/JoueurMajoMou.c 
	$(CC) -c -o tests/JoueurMajoMou.o tests/JoueurMajoMou.c $(CFLAGS)

tests/JoueurMechant.o: tests/JoueurMechant.c 
	$(CC) -c -o tests/JoueurMechant.o tests/JoueurMechant.c $(CFLAGS)

tests/JoueurMefiant.o: tests/JoueurMefiant.c 
	$(CC) -c -o tests/JoueurMefiant.o tests/JoueurMefiant.c $(CFLAGS)

tests/JoueurPavlov.o: tests/JoueurPavlov.c 
	$(CC) -c -o tests/JoueurPavlov.o tests/JoueurPavlov.c $(CFLAGS)

tests/JoueurCCT.o: tests/JoueurCCT.c 
	$(CC) -c -o tests/JoueurCCT.o tests/JoueurCCT.c $(CFLAGS)

tests/JoueurTTC.o: tests/JoueurTTC.c 
	$(CC) -c -o tests/JoueurTTC.o tests/JoueurTTC.c $(CFLAGS)

tests/JoueurRancunier.o: tests/JoueurRancunier.c 
	$(CC) -c -o tests/JoueurRancunier.o tests/JoueurRancunier.c $(CFLAGS)

tests/JoueurSondeur.o: tests/JoueurSondeur.c 
	$(CC) -c -o tests/JoueurSondeur.o tests/JoueurSondeur.c $(CFLAGS)

tests/JoueurDetective.o: tests/JoueurDetective.c 
	$(CC) -c -o tests/JoueurDetective.o tests/JoueurDetective.c $(CFLAGS)

test.o: test.c 
	$(CC) -c -o test.o test.c $(CFLAGS)

prisonnier: prisonnier.o \
	common/common_empty_functions.o \
	common/Arene.o \
	common/GestionEffectif.o \
	joueurs/JoueurGentil.o \
	joueurs/JoueurMechant.o \
	joueurs/JoueurLunatique.o \
	joueurs/JoueurDonnantDonnant.o \
	joueurs/JoueurMefiant.o \
	joueurs/JoueurDonnantDonnantDur.o  \
	joueurs/JoueurGraduel.o	\
	joueurs/JoueurMajoMou.o	\
	joueurs/JoueurMajoDur.o \
	joueurs/JoueurPavlov.o \
	joueurs/JoueurCCT.o \
	joueurs/JoueurTTC.o \
	joueurs/JoueurRancunier.o \
	joueurs/JoueurSondeur.o \
	joueurs/JoueurDetective.o \
	joueurs/declarerJoueurs.o \
	common/MoteurJeu.o \
	sorties.o

	$(CC) -o prisonnier \
		prisonnier.o \
		common/common_empty_functions.o \
		common/Arene.o \
		common/GestionEffectif.o \
		joueurs/JoueurGentil.o \
		joueurs/JoueurMechant.o \
		joueurs/JoueurLunatique.o \
		joueurs/JoueurDonnantDonnant.o \
		joueurs/JoueurMefiant.o \
		joueurs/JoueurDonnantDonnantDur.o  \
		joueurs/JoueurGraduel.o	\
		joueurs/JoueurMajoMou.o	\
		joueurs/JoueurMajoDur.o \
		joueurs/JoueurPavlov.o \
		joueurs/JoueurCCT.o \
		joueurs/JoueurTTC.o \
		joueurs/JoueurRancunier.o \
		joueurs/JoueurSondeur.o \
		joueurs/JoueurDetective.o \
		joueurs/declarerJoueurs.o \
		common/MoteurJeu.o \
		sorties.o


prisonnier.o: prisonnier.c
	$(CC) -c -o prisonnier.o prisonnier.c $(CFLAGS)
	
common/Arene.o: common/Arene.c
	$(CC) -c -o common/Arene.o common/Arene.c $(CFLAGS)

common/GestionEffectif.o: common/GestionEffectif.c
	$(CC) -c -o common/GestionEffectif.o common/GestionEffectif.c $(CFLAGS)

sorties.o: sorties.c
	$(CC) -c -o sorties.o sorties.c $(CFLAGS)



clean:
	rm -rf *.o
	rm -Rf joueurs/*.o
	rm -Rf common/*.o
	rm -Rf tests/*.o

mrproper:
	rm -f test
	rm -f prisonnier
