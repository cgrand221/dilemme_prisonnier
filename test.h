

#ifndef MAIN_H
#define MAIN_H


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "common/definitions.h"
#include "common/common_empty_functions.h"
#include "joueurs/JoueurGentil.h"
#include "joueurs/JoueurMechant.h"
#include "joueurs/JoueurLunatique.h"
#include "joueurs/JoueurDonnantDonnant.h"
#include "joueurs/JoueurMefiant.h"
#include "joueurs/JoueurDonnantDonnantDur.h"
#include "joueurs/JoueurGraduel.h"
#include "joueurs/JoueurMajoMou.h"
#include "joueurs/JoueurMajoDur.h"
#include "joueurs/JoueurPavlov.h"
#include "joueurs/JoueurCCT.h"
#include "joueurs/JoueurTTC.h"
#include "joueurs/JoueurRancunier.h"
#include "joueurs/JoueurSondeur.h"
#include "joueurs/JoueurDetective.h"
#include "common/MoteurJeu.h"

#include "tests/commonTest.h"
#include "tests/JoueurDonnantDonnant.h"
#include "tests/JoueurDonnantDonnantDur.h"
#include "tests/JoueurGentil.h"
#include "tests/JoueurGraduel.h"
#include "tests/JoueurLunatique.h"
#include "tests/JoueurMajoDur.h"
#include "tests/JoueurMajoMou.h"
#include "tests/JoueurMechant.h"
#include "tests/JoueurMefiant.h"
#include "tests/JoueurPavlov.h"
#include "tests/JoueurCCT.h"
#include "tests/JoueurTTC.h"
#include "tests/JoueurRancunier.h"
#include "tests/JoueurSondeur.h"
#include "tests/JoueurDetective.h"


#endif /* MAIN_H */

