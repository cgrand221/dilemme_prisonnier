
#include "test.h"


void test(){
    int tabGain[8];
    
    initMoteur(
        tabGain,
        /*
        * gains du joueur 1
        * \J2!   !   !
        *  \ ! T ! C !
        * J1\!   !   !
        * ---!---!---!
        *  T !   !   !
        * ---!---!---!
        *  C !   !   !
        */   
        1, 5,
        0, 3,
            
        /*
         * gains du joueur 2
         * \J2!   !   !
         *  \ ! T ! C !
         * J1\!   !   !
         * ---!---!---!
         *  T !   !   !
         * ---!---!---!
         *  C !   !   !
         */   
        1, 0,
        5, 3
    );
    etapeJeu * ej = (etapeJeu *) malloc(sizeof(*ej));
    memoireJoueur * mj = (memoireJoueur *) malloc (sizeof(*mj));
    
    
    testJoueurDonnantDonnant(ej, mj, tabGain);
    testJoueurDonnantDonnantDur(ej, mj, tabGain);
    testJoueurGentil(ej, mj, tabGain);
    testJoueurGraduel(ej, mj, tabGain);
    testJoueurLunatique(ej, mj, tabGain);
    testJoueurMajoDur(ej, mj, tabGain);
    testJoueurMajoMou(ej, mj, tabGain);
    testJoueurMechant(ej, mj, tabGain);
    testJoueurMefiant(ej, mj, tabGain);
    testJoueurPavlov(ej, mj, tabGain);
    testJoueurCCT(ej, mj, tabGain);
    testJoueurTTC(ej, mj, tabGain);
    testJoueurRancunier(ej, mj, tabGain);
    testJoueurSondeur(ej, mj, tabGain);
    testJoueurDetective(ej, mj, tabGain);
    
    free(ej);
    free(mj);
}




int main(int argc, char* argv[]) {
    test();
    return (EXIT_SUCCESS);
}

