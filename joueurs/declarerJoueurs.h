

#ifndef DECLARERJOUEURS_H
#define DECLARERJOUEURS_H

#include "../common/definitions.h"
#include "../joueurs/JoueurGentil.h"
#include "../joueurs/JoueurMechant.h"
#include "../joueurs/JoueurLunatique.h"
#include "../joueurs/JoueurDonnantDonnant.h"
#include "../joueurs/JoueurMefiant.h"
#include "../joueurs/JoueurDonnantDonnantDur.h"
#include "../joueurs/JoueurGraduel.h"
#include "../joueurs/JoueurMajoMou.h"
#include "../joueurs/JoueurMajoDur.h"
#include "../joueurs/JoueurPavlov.h"
#include "../joueurs/JoueurCCT.h"
#include "../joueurs/JoueurTTC.h"
#include "../joueurs/JoueurRancunier.h"
#include "../joueurs/JoueurSondeur.h"
#include "../joueurs/JoueurDetective.h" 

#define TAILLEBUFF 255
#define NBTYPEJOUEUR 15


void listeEtapeJoueurs( void (**tabSetEtapeJoueur)(etapeJeu *));

#endif /* DECLARERJOUEURS_H */

