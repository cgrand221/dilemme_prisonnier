

#ifndef JOUEURMEFIANT_H
#define JOUEURMEFIANT_H

#include "../common/definitions.h"
#include "JoueurDonnantDonnant.h"

void initJoueurMefiant(memoireJoueur * mj);
void setEtapeJoueurMefiant(etapeJeu * ej);

#endif /* JOUEURMEFIANT_H */

