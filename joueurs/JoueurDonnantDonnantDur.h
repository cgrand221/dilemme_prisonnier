
#ifndef JOUEURDONNANTDONNANTDUR_H
#define JOUEURDONNANTDONNANTDUR_H

#include "../common/definitions.h"

#define PARAM_DONNANT_DONNANT_DUR 2

void initJoueurDonnantDonnantDur(memoireJoueur * mj);
int jouerJoueurDonnantDonnantDur(memoireJoueur * mj);
void setResultatConfrontationJoueurDonnantDonnantDur(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
void setEtapeJoueurDonnantDonnantDur(etapeJeu * ej);

#endif /* JOUEURDONNANTDONNANTDUR_H */

