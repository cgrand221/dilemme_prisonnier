

#ifndef JOUEURDONNANTDONNANT_H
#define JOUEURDONNANTDONNANT_H

#include "../common/definitions.h"

void initJoueurDonnantDonnant(memoireJoueur * mj);
int jouerJoueurDonnantDonnantOuMefiant(memoireJoueur  * mj);
void setResultatConfrontationJoueurDonnantDonnantOuMefiant(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
void setEtapeJoueurDonnantDonnant(etapeJeu * ej);

#endif /* JOUEURDONNANTDONNANT_H */

