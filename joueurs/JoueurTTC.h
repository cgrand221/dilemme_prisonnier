
#ifndef JOUEURTTC_H
#define JOUEURTTC_H

#include "../common/definitions.h"
#include "JoueurCCT.h"

int jouerJoueurTTC(memoireJoueur * mj);
void setEtapeJoueurTTC(etapeJeu * ej);

#endif /* JOUEURTTC_H */

