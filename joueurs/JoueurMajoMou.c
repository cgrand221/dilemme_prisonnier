#include "JoueurMajoMou.h"

void initJoueurMajoMouOuMajoDur(memoireJoueur * mj){
    mj->nbCooperationsTotalAdversaire = 0;
    mj->nbTrahisonsTotalAdversaire = 0;
}
int jouerJoueurMajoMou(memoireJoueur * mj){
    if(mj->nbCooperationsTotalAdversaire >= mj->nbTrahisonsTotalAdversaire){
        return COOPERER;
    }
    return TRAHIR;
}
void setResultatConfrontationJoueurMajoMouOuMajoDur(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire){
    if(coupAdversaire == COOPERER){
        mj->nbCooperationsTotalAdversaire++;
    }
    else{
        mj->nbTrahisonsTotalAdversaire++;
    }
}
void setEtapeJoueurMajoMou(etapeJeu * ej){
    ej->init = initJoueurMajoMouOuMajoDur;
    ej->jouerJoueur = jouerJoueurMajoMou;
    ej->setResultatConfrontation = setResultatConfrontationJoueurMajoMouOuMajoDur;
    ej->nomJoueur = "JoueurMajoMou";
    ej->estAleatoire = 0;
}