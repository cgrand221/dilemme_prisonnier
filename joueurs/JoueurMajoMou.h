

#ifndef JOUEURMAJOMOU_H
#define JOUEURMAJOMOU_H

#include "../common/definitions.h"
void initJoueurMajoMouOuMajoDur(memoireJoueur * mj);
int jouerJoueurMajoMou(memoireJoueur * mj);
void setResultatConfrontationJoueurMajoMouOuMajoDur(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
void setEtapeJoueurMajoMou(etapeJeu * ej);

#endif /* JOUEURMAJOMOU_H */

