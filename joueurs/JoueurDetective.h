
#ifndef JOUEURDETECTIVE_H
#define JOUEURDETECTIVE_H

#include "../common/definitions.h"

#define PARAM_DETECTIVE_COUP_3_PAS_TRAHI 0
#define PARAM_DETECTIVE_COUP_3_TRAHI 1

void initJoueurDetective(memoireJoueur * mj);
int jouerJoueurDetective(memoireJoueur * mj);
void setResultatConfrontationJoueurDetective(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
void setEtapeJoueurDetective(etapeJeu * ej);

#endif /* JOUEURDETECTIVE_H */

