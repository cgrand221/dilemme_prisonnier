
#ifndef JOUEURLUNATIQUE_H
#define JOUEURLUNATIQUE_H

#include <stdlib.h>
#include "../common/definitions.h"
#include "../common/common_empty_functions.h"

int jouerJoueurLunatique(memoireJoueur * mj);
void setEtapeJoueurLunatique(etapeJeu * ej);

#endif /* JOUEURLUNATIQUE_H */

