#include "JoueurDonnantDonnant.h"

void initJoueurDonnantDonnant(memoireJoueur * mj){
    mj->dernierCoupAdversaire = COOPERER;
} 
int jouerJoueurDonnantDonnantOuMefiant(memoireJoueur  * mj){
    return mj->dernierCoupAdversaire;
}
void setResultatConfrontationJoueurDonnantDonnantOuMefiant(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire){
    mj->dernierCoupAdversaire = coupAdversaire;
}
void setEtapeJoueurDonnantDonnant(etapeJeu * ej){
    ej->init = initJoueurDonnantDonnant;
    ej->jouerJoueur = jouerJoueurDonnantDonnantOuMefiant;
    ej->setResultatConfrontation = setResultatConfrontationJoueurDonnantDonnantOuMefiant;
    ej->nomJoueur = "JoueurDonnantDonnant";
    ej->estAleatoire = 0;
}