

#ifndef JOUEURCCT_H
#define JOUEURCCT_H

#include "../common/definitions.h"
#include "../common/common_empty_functions.h"

#define PARAM_CCT 3

void initJoueurCCTOuTTC(memoireJoueur * mj);
int jouerJoueurCCT(memoireJoueur * mj);
void setEtapeJoueurCCT(etapeJeu * ej);

#endif /* JOUEURCCT_H */

