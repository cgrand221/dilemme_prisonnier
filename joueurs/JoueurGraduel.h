

#ifndef JOUEURGRADUEL_H
#define JOUEURGRADUEL_H

#include "../common/definitions.h"


#define PARAM_GRADUEL_DUREE_PAIX_FORCEE 2

void initJoueurGraduel(memoireJoueur * mj);
int jouerJoueurGraduel(memoireJoueur * mj);
void setResultatConfrontationJoueurGraduel(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
void setEtapeJoueurGraduel(etapeJeu * ej);

#endif /* JOUEURGRADUEL_H */

