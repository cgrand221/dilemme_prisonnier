#include "JoueurGraduel.h"


void initJoueurGraduel(memoireJoueur * mj){
    mj->nbTrahisonMoiEnCours = -PARAM_GRADUEL_DUREE_PAIX_FORCEE;
    mj->nbTrahisonsTotalAdversaire = 0;
}
int jouerJoueurGraduel(memoireJoueur * mj){
    int retour = COOPERER;
    
    if(mj->nbTrahisonMoiEnCours == -PARAM_GRADUEL_DUREE_PAIX_FORCEE){
        retour = COOPERER;
    }
    else {
        if(mj->nbTrahisonMoiEnCours > 0){
            retour = TRAHIR;
        }
        else{
            retour = COOPERER;
        }
        mj->nbTrahisonMoiEnCours--;
    }
    return retour;
}
void setResultatConfrontationJoueurGraduel(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire){
    if(mj->nbTrahisonMoiEnCours == -PARAM_GRADUEL_DUREE_PAIX_FORCEE){
        if(coupAdversaire == TRAHIR){
            mj->nbTrahisonsTotalAdversaire++;
            mj->nbTrahisonMoiEnCours =  mj->nbTrahisonsTotalAdversaire;
        }
    }
}
void setEtapeJoueurGraduel(etapeJeu * ej){
    ej->init = initJoueurGraduel;
    ej->jouerJoueur = jouerJoueurGraduel;
    ej->setResultatConfrontation = setResultatConfrontationJoueurGraduel;
    ej->nomJoueur = "JoueurGraduel";
    ej->estAleatoire = 0;
}
