#include "JoueurLunatique.h"

int jouerJoueurLunatique(memoireJoueur * mj){
    return rand()%2;
}
void setEtapeJoueurLunatique(etapeJeu * ej){
    ej->init = initJoueurFaitRien;
    ej->jouerJoueur = jouerJoueurLunatique;
    ej->setResultatConfrontation = setResultatConfrontationFaitRien;
    ej->nomJoueur = "JoueurLunatique";
    ej->estAleatoire = 1;
}
