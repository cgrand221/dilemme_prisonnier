

#ifndef JOUEURMAJODUR_H
#define JOUEURMAJODUR_H

#include "../common/definitions.h"
#include "JoueurMajoMou.h"

int jouerJoueurMajoDur(memoireJoueur * mj);
void setEtapeJoueurMajoDur(etapeJeu * ej);

#endif /* JOUEURMAJODUR_H */

