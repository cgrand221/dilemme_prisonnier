#include "JoueurMechant.h"


int jouerJoueurMechant(memoireJoueur * mj){
    return TRAHIR;
}
void setEtapeJoueurMechant(etapeJeu * ej){
    ej->init = initJoueurFaitRien;
    ej->jouerJoueur = jouerJoueurMechant;
    ej->setResultatConfrontation = setResultatConfrontationFaitRien;
    ej->nomJoueur = "JoueurMechant";
    ej->estAleatoire = 0;
}