#include "JoueurRancunier.h"

int jouerJoueurRancunier(memoireJoueur * mj){
    return mj->dernierCoupAdversaire;
}
void setResultatConfrontationJoueurRancunier(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire){
    if(mj->dernierCoupAdversaire == COOPERER){
        mj->dernierCoupAdversaire = coupAdversaire;
    }
}
void setEtapeJoueurRancunier(etapeJeu * ej){
    ej->init = initJoueurDonnantDonnant;
    ej->jouerJoueur = jouerJoueurRancunier;
    ej->setResultatConfrontation = setResultatConfrontationJoueurRancunier;
    ej->nomJoueur = "JoueurRancunier";
    ej->estAleatoire = 0;
}