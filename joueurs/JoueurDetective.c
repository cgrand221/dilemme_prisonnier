#include "JoueurDetective.h"


void initJoueurDetective(memoireJoueur * mj){
    mj->nbConfrontation = 0;
    mj->etatConfrontation = PARAM_DETECTIVE_COUP_3_PAS_TRAHI;
} 
int jouerJoueurDetective(memoireJoueur * mj){
    mj->nbConfrontation++;
    if((mj->nbConfrontation  == 1) || (mj->nbConfrontation  == 3)  || (mj->nbConfrontation  == 4)){
        return COOPERER;
    }
    else if(mj->nbConfrontation  == 2){
        return TRAHIR;
    }
    
    return mj->dernierCoupAdversaire;
}
void setResultatConfrontationJoueurDetective(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire){
    if(mj->nbConfrontation == 3){
        if(coupAdversaire == TRAHIR){
            // DONNANT DONNANT
            mj->etatConfrontation = PARAM_DETECTIVE_COUP_3_TRAHI;
        }
    }
    if(mj->etatConfrontation == PARAM_DETECTIVE_COUP_3_TRAHI){
        mj->dernierCoupAdversaire = coupAdversaire;
    }
    else mj->dernierCoupAdversaire = TRAHIR;
}
void setEtapeJoueurDetective(etapeJeu * ej){
    ej->init = initJoueurDetective;
    ej->jouerJoueur = jouerJoueurDetective;
    ej->setResultatConfrontation = setResultatConfrontationJoueurDetective;
    ej->nomJoueur = "JoueurDetective";
    ej->estAleatoire = 0;
}