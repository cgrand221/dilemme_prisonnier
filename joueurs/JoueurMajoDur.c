#include "JoueurMajoDur.h"

int jouerJoueurMajoDur(memoireJoueur * mj){
    if(mj->nbCooperationsTotalAdversaire > mj->nbTrahisonsTotalAdversaire){
        return COOPERER;
    }
    return TRAHIR;
}
void setEtapeJoueurMajoDur(etapeJeu * ej){
    ej->init = initJoueurMajoMouOuMajoDur;
    ej->jouerJoueur = jouerJoueurMajoDur;
    ej->setResultatConfrontation = setResultatConfrontationJoueurMajoMouOuMajoDur;
    ej->nomJoueur = "JoueurMajoDur";
    ej->estAleatoire = 0;
}
