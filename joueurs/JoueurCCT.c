#include "JoueurCCT.h"

void initJoueurCCTOuTTC(memoireJoueur * mj){
    mj->nbConfrontation = 0;
}
int jouerJoueurCCT(memoireJoueur * mj){
    mj->nbConfrontation++;
    if(mj->nbConfrontation % PARAM_CCT == 0){
        return TRAHIR;
    }
    return COOPERER;
}
void setEtapeJoueurCCT(etapeJeu * ej){
    ej->init = initJoueurCCTOuTTC;
    ej->jouerJoueur = jouerJoueurCCT;
    ej->setResultatConfrontation = setResultatConfrontationFaitRien;
    ej->nomJoueur = "JoueurCCT";
    ej->estAleatoire = 0;
}