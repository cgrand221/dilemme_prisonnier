#include "JoueurPavlov.h"

void initJoueurPavlov(memoireJoueur * mj){
    mj->gainMoiAuDernierCoup = PARAM_PAVLOV;
    mj->jeuMoiAuDernierCoup = COOPERER;
}
int jouerJoueurPavlov(memoireJoueur * mj){
    if(mj->gainMoiAuDernierCoup >= PARAM_PAVLOV){
        return mj->jeuMoiAuDernierCoup;
    }
    return COOPERER-mj->jeuMoiAuDernierCoup;
}
void setResultatConfrontationJoueurPavlov(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire){
    mj->jeuMoiAuDernierCoup = monCoup;
    mj->gainMoiAuDernierCoup = monGain;
}
void setEtapeJoueurPavlov(etapeJeu * ej){
    ej->init = initJoueurPavlov;
    ej->jouerJoueur = jouerJoueurPavlov;
    ej->setResultatConfrontation = setResultatConfrontationJoueurPavlov;
    ej->nomJoueur = "JoueurPavlov";
    ej->estAleatoire = 0;
}