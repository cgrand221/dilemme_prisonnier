
#ifndef JOUEURPAVLOV_H
#define JOUEURPAVLOV_H

#include "../common/definitions.h"

#define PARAM_PAVLOV 3

void initJoueurPavlov(memoireJoueur * mj);
int jouerJoueurPavlov(memoireJoueur * mj);
void setResultatConfrontationJoueurPavlov(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
void setEtapeJoueurPavlov(etapeJeu * ej);

#endif /* JOUEURPAVLOV_H */

