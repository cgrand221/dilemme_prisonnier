#include "JoueurSondeur.h"

void initJoueurSondeur(memoireJoueur * mj){
    mj->nbConfrontation = 0;
    mj->etatConfrontation = PARAM_SONDEUR_COUP_2_3_PAS_TRAHI;
    mj->dernierCoupAdversaire = COOPERER;
}
int jouerJoueurSondeur(memoireJoueur * mj){
    mj->nbConfrontation++;
    if(mj->nbConfrontation == 1){
        return TRAHIR;
    }
    if(mj->nbConfrontation < 4){
        return COOPERER;
    }
    if(mj->etatConfrontation == PARAM_SONDEUR_COUP_2_3_PAS_TRAHI){
        return TRAHIR;
    }
    return mj->dernierCoupAdversaire;
}
void setResultatConfrontationJoueurSondeur(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire){
    if(mj->etatConfrontation == PARAM_SONDEUR_COUP_2_3_PAS_TRAHI){
        if((mj->nbConfrontation == 2) || (mj->nbConfrontation == 3)){
            if(coupAdversaire == TRAHIR){
                mj->etatConfrontation = PARAM_SONDEUR_COUP_2_3_TRAHI;
            }
        }
    }
    mj->dernierCoupAdversaire = coupAdversaire;
}
void setEtapeJoueurSondeur(etapeJeu * ej){
    ej->init = initJoueurSondeur;
    ej->jouerJoueur = jouerJoueurSondeur;
    ej->setResultatConfrontation = setResultatConfrontationJoueurSondeur;
    ej->nomJoueur = "JoueurSondeur";
    ej->estAleatoire = 0;
}