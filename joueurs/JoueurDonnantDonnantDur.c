#include "JoueurDonnantDonnantDur.h"


void initJoueurDonnantDonnantDur(memoireJoueur * mj){
    mj->nbTrahirProchainsCoups = 0;
} 
int jouerJoueurDonnantDonnantDur(memoireJoueur * mj){
    if(mj->nbTrahirProchainsCoups > 0){
        return TRAHIR;
    }
    return COOPERER;
}
void setResultatConfrontationJoueurDonnantDonnantDur(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire){
    if(coupAdversaire == TRAHIR){
        mj->nbTrahirProchainsCoups = PARAM_DONNANT_DONNANT_DUR;
    }
    else if (mj->nbTrahirProchainsCoups > 0){
        mj->nbTrahirProchainsCoups--;
    }
}
void setEtapeJoueurDonnantDonnantDur(etapeJeu * ej){
    ej->init = initJoueurDonnantDonnantDur;
    ej->jouerJoueur = jouerJoueurDonnantDonnantDur;
    ej->setResultatConfrontation = setResultatConfrontationJoueurDonnantDonnantDur;
    ej->nomJoueur = "JoueurDonnantDonnantDur";
    ej->estAleatoire = 0;
}