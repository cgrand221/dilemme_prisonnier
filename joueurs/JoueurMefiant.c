#include "JoueurMefiant.h"


void initJoueurMefiant(memoireJoueur * mj){
    mj->dernierCoupAdversaire = TRAHIR;
} 
void setEtapeJoueurMefiant(etapeJeu * ej){
    ej->init = initJoueurMefiant;
    ej->jouerJoueur = jouerJoueurDonnantDonnantOuMefiant;
    ej->setResultatConfrontation = setResultatConfrontationJoueurDonnantDonnantOuMefiant;
    ej->nomJoueur = "JoueurMefiant";
    ej->estAleatoire = 0;
}
