
#ifndef JOUEURRANCUNIER_H
#define JOUEURRANCUNIER_H

#include "../common/definitions.h"
#include "JoueurDonnantDonnant.h"

int jouerJoueurRancunier(memoireJoueur * mj);
void setResultatConfrontationJoueurRancunier(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
void setEtapeJoueurRancunier(etapeJeu * ej);

#endif /* JOUEURRANCUNIER_H */

