#include "JoueurGentil.h"

int jouerJoueurGentil(memoireJoueur * mj){
    return COOPERER;
}


void setEtapeJoueurGentil(etapeJeu * ej){
    ej->init = initJoueurFaitRien;
    ej->jouerJoueur = jouerJoueurGentil;
    ej->setResultatConfrontation = setResultatConfrontationFaitRien;
    ej->nomJoueur = "JoueurGentil";
    ej->estAleatoire = 0;
}