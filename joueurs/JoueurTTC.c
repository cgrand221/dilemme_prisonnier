#include "JoueurTTC.h"


int jouerJoueurTTC(memoireJoueur * mj){
    mj->nbConfrontation++;
    if(mj->nbConfrontation % PARAM_CCT == 0){
        return COOPERER;
    }
    return TRAHIR;
}

void setEtapeJoueurTTC(etapeJeu * ej){
    ej->init = initJoueurCCTOuTTC;
    ej->jouerJoueur = jouerJoueurTTC;
    ej->setResultatConfrontation = setResultatConfrontationFaitRien;
    ej->nomJoueur = "JoueurTTC";
    ej->estAleatoire = 0;
}