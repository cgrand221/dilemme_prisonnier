
#ifndef JOUEURMECHANT_H
#define JOUEURMECHANT_H

#include "../common/definitions.h"
#include "../common/common_empty_functions.h"

int jouerJoueurMechant(memoireJoueur * mj);
void setEtapeJoueurMechant(etapeJeu * ej);

#endif /* JOUEURMECHANT_H */

