

#ifndef JOUEURSONDEUR_H
#define JOUEURSONDEUR_H

#include "../common/definitions.h"

#define PARAM_SONDEUR_COUP_2_3_PAS_TRAHI 0
#define PARAM_SONDEUR_COUP_2_3_TRAHI 1


void initJoueurSondeur(memoireJoueur * mj);
int jouerJoueurSondeur(memoireJoueur * mj);
void setResultatConfrontationJoueurSondeur(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
void setEtapeJoueurSondeur(etapeJeu * ej);

#endif /* JOUEURSONDEUR_H */

