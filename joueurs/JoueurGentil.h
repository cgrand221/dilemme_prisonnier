

#ifndef JOUEURGENTIL_H
#define JOUEURGENTIL_H

#include "../common/definitions.h"
#include "../common/common_empty_functions.h"

int jouerJoueurGentil(memoireJoueur * mj);
void setEtapeJoueurGentil(etapeJeu * ej);


#endif /* JOUEURGENTIL_H */

