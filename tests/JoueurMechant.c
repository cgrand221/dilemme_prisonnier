#include "JoueurMechant.h"

void testJoueurMechant(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int i;
    setEtapeJoueurMechant(ej);
    ej->init(mj);
    for(i = 0;i < 100;i++){
        int jeuJoueurJoue = ej->jouerJoueur(mj);
        int jeuJoueurAttendu = TRAHIR;
        int jeuAdversaire = rand()%2;
        int * gainJoueurs = getGainJoueur1Joueur2(tabGain, jeuJoueurJoue, jeuAdversaire);
        int joueurGain = *gainJoueurs;
        int adversaireGain = *(gainJoueurs+1);
        ej->setResultatConfrontation (mj, jeuJoueurJoue, jeuAdversaire, joueurGain, adversaireGain);
        if(jeuJoueurJoue != jeuJoueurAttendu){
            phrase_erreur("JoueurMechant", 1, i+1, jeuJoueurJoue);
        }
    }
}