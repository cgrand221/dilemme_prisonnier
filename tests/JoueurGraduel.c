#include "JoueurGraduel.h"

void testJoueurGraduel(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 24;
    setEtapeJoueurGraduel(ej);   
    int tabTest3[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,/*adversaire*/
        1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, /*adversaire*/
        1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1 /*joueur*/
    };
    testData(tabTest3, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}