

#ifndef TEST_JOUEURRANCUNIER_H
#define TEST_JOUEURRANCUNIER_H

#include "../common/definitions.h"
#include "../joueurs/JoueurRancunier.h"
#include "commonTest.h"

void testJoueurRancunier(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURRANCUNIER_H */

