

#ifndef TEST_JOUEURSONDEUR_H
#define TEST_JOUEURSONDEUR_H

#include "../common/definitions.h"
#include "../joueurs/JoueurSondeur.h"
#include "commonTest.h"

void testJoueurSondeur(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURSONDEUR_H */

