#include "commonTest.h"


void phrase_erreur(const char * typeJoueur, int numeroTest, int numeroCoup, int jeuJoueurJoue){
    char *strJeuJoueurJoue, *strJeuJoueurAttendu;
    if(jeuJoueurJoue == COOPERER){ 
        strJeuJoueurJoue = "COOPERER";
        strJeuJoueurAttendu = "TRAHIR";
    }
    else{
        strJeuJoueurJoue = "TRAHIR";
        strJeuJoueurAttendu = "COOPERER";
    }
    printf("%s n'a pas joue le coup attendu au test n° %d, au coup n° %d, il a joue %s alors qu'il aurait du jouer %s\n", typeJoueur, numeroTest, numeroCoup, strJeuJoueurJoue, strJeuJoueurAttendu);
    
}



void testData(int * tabTest, int nbTest, int nbConfrontationParTest, etapeJeu * ej, memoireJoueur * mj, int * tabGain){
    int i,j;
    
    for(i=0;i < nbTest;i++){
      
        ej->init(mj);
        for(j = 0;j < nbConfrontationParTest;j++){
            int jeuAdversaire = tabTest[i*nbConfrontationParTest*2+j];
            int jeuJoueurAttendu = tabTest[i*nbConfrontationParTest*2+j+nbConfrontationParTest];
            int jeuJoueurJoue = ej->jouerJoueur(mj);
            
            int * gainJoueurs = getGainJoueur1Joueur2(tabGain, jeuJoueurJoue, jeuAdversaire);
            int joueurGain = *gainJoueurs;
            int adversaireGain = *(gainJoueurs+1);
            
            /*printf("%d %d %d\n", jeuAdversaire, jeuJoueurAttendu, jeuJoueurJoue);*/
            
            ej->setResultatConfrontation(mj, jeuJoueurJoue, jeuAdversaire, joueurGain, adversaireGain);
            
            if(jeuJoueurJoue != jeuJoueurAttendu){
                phrase_erreur(ej->nomJoueur, i+1, j+1, jeuJoueurJoue);
            }
            
        }
        /*printf("\n");*/
    }
}