

#ifndef TEST_JOUEURMFIANT_H
#define TEST_JOUEURMFIANT_H

#include "../common/definitions.h"
#include "../joueurs/JoueurMefiant.h"
#include "commonTest.h"

void testJoueurMefiant(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURMFIANT_H */

