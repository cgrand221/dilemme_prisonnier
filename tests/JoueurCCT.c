#include "JoueurCCT.h"

void testJoueurCCT(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurCCT(ej);   
    int tabTest8[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0 /*joueur*/
    };
    testData(tabTest8, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}