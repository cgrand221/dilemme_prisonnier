

#ifndef TEST_JOUEURMAJOMOU_H
#define TEST_JOUEURMAJOMOU_H

#include "../common/definitions.h"
#include "../joueurs/JoueurMajoMou.h"
#include "commonTest.h"

void testJoueurMajoMou(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURMAJOMOU_H */

