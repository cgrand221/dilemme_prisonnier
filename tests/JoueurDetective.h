

#ifndef TEST_JOUEURDETECTIVE_H
#define TEST_JOUEURDETECTIVE_H

#include "../common/definitions.h"
#include "../joueurs/JoueurDetective.h"
#include "commonTest.h"

void testJoueurDetective(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURDETECTIVE_H */

