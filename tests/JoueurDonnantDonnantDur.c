#include "JoueurDonnantDonnantDur.h"

void testJoueurDonnantDonnantDur(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurDonnantDonnantDur(ej);
    int tabTest2[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,/*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1 /*joueur*/
    };
    testData(tabTest2, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}