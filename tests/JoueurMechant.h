

#ifndef TEST_JOUEURMECHANT_H
#define TEST_JOUEURMECHANT_H

#include <stdlib.h>
#include "../common/definitions.h"
#include "../joueurs/JoueurMechant.h"
#include "commonTest.h"

void testJoueurMechant(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURMECHANT_H */

