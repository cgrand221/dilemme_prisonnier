#include "JoueurLunatique.h"

void testJoueurLunatique(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int i;
    setEtapeJoueurLunatique(ej);
    ej->init(mj);
    for(i = 0;i < 100;i++){
        int jeuJoueurJoue = ej->jouerJoueur(mj);
        int jeuAdversaire = rand()%2;
        int * gainJoueurs = getGainJoueur1Joueur2(tabGain, jeuJoueurJoue, jeuAdversaire);
        int joueurGain = *gainJoueurs;
        int adversaireGain = *(gainJoueurs+1);
        ej->setResultatConfrontation (mj, jeuJoueurJoue, jeuAdversaire, joueurGain, adversaireGain);
    }
}