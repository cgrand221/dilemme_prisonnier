#include "JoueurTTC.h"

void testJoueurTTC(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurTTC(ej);   
    int tabTest9[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 /*joueur*/
    };
    testData(tabTest9, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}