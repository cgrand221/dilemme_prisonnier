

#ifndef TEST_JOUEURPAVLOV_H
#define TEST_JOUEURPAVLOV_H

#include "../common/definitions.h"
#include "../joueurs/JoueurPavlov.h"
#include "commonTest.h"

void testJoueurPavlov(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURPAVLOV_H */

