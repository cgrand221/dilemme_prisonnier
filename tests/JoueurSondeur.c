#include "JoueurSondeur.h"

void testJoueurSondeur(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurSondeur(ej);   
    int tabTest11[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, /*joueur*/
        
        1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, /*adversaire*/
        0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 /*joueur*/
    };
    testData(tabTest11, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}