

#ifndef TEST_JOUEURDONNANTDONNANTDUR_H
#define TEST_JOUEURDONNANTDONNANTDUR_H

#include "../common/definitions.h"
#include "../joueurs/JoueurDonnantDonnantDur.h"
#include "commonTest.h"

void testJoueurDonnantDonnantDur(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURDONNANTDONNANTDUR_H */

