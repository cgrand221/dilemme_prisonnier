

#ifndef TEST_JOUEURLUNATIQUE_H
#define TEST_JOUEURLUNATIQUE_H

#include "../common/definitions.h"
#include "../joueurs/JoueurLunatique.h"
#include "commonTest.h"

void testJoueurLunatique(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURLUNATIQUE_H */

