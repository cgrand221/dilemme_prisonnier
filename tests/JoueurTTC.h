

#ifndef TEST_JOUEURTTC_H
#define TEST_JOUEURTTC_H

#include "../common/definitions.h"
#include "../joueurs/JoueurTTC.h"
#include "commonTest.h"

void testJoueurTTC(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURTTC_H */

