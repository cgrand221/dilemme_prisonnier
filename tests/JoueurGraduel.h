

#ifndef TEST_JOUEURGRADUEL_H
#define TEST_JOUEURGRADUEL_H

#include "../common/definitions.h"
#include "../joueurs/JoueurGraduel.h"
#include "commonTest.h"

void testJoueurGraduel(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURGRADUEL_H */

