#include "JoueurPavlov.h"

void testJoueurPavlov(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurPavlov(ej);   
    int tabTest7[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0 /*joueur*/
    };
    testData(tabTest7, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}