

#ifndef TEST_JOUEURMAJODUR_H
#define TEST_JOUEURMAJODUR_H

#include "../common/definitions.h"
#include "../joueurs/JoueurMajoDur.h"
#include "commonTest.h"

void testJoueurMajoDur(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURMAJODUR_H */

