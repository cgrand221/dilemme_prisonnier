

#ifndef TEST_JOUEURGENTIL_H
#define TEST_JOUEURGENTIL_H

#include <stdlib.h>
#include "../common/definitions.h"
#include "../joueurs/JoueurGentil.h"
#include "commonTest.h"

void testJoueurGentil(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURGENTIL_H */

