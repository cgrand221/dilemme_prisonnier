#include "JoueurMajoDur.h"

void testJoueurMajoDur(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurMajoDur(ej);   
    int tabTest4[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 /*joueur*/
    };
    testData(tabTest4, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}