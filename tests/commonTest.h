

#ifndef COMMONTEST_H
#define COMMONTEST_H

#include <stdio.h>
#include "../common/definitions.h"
#include "../common/MoteurJeu.h"

void phrase_erreur(const char * typeJoueur, int numeroTest, int numeroCoup, int jeuJoueurJoue);
void testData(int * tabTest, int nbTest, int nbConfrontationParTest, etapeJeu * ej, memoireJoueur * mj, int * tabGain);

#endif /* COMMONTEST_H */

