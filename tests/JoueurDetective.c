#include "JoueurDetective.h"

void testJoueurDetective(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 3;
    int nbConfrontationParTest = 24;
    setEtapeJoueurDetective(ej);   
    int tabTest3[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, /*adversaire*/
        1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, /*adversaire*/
        1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, /*joueur*/
        
        0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, /*adversaire*/
        1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /*joueur*/
    };
    testData(tabTest3, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}