#include "JoueurDonnantDonnant.h"



void testJoueurDonnantDonnant(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurDonnantDonnant(ej);   
    int tabTest[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0,/*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1 /*joueur*/
    };
    testData(tabTest, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}