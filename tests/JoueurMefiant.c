#include "JoueurMefiant.h"

void testJoueurMefiant(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurMefiant(ej);   
    int tabTest6[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1 /*joueur*/
    };
    testData(tabTest6, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}