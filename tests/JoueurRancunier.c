#include "JoueurRancunier.h"

void testJoueurRancunier(etapeJeu * ej, memoireJoueur * mj, int *tabGain){
    int nbTest = 2;
    int nbConfrontationParTest = 12;
    setEtapeJoueurRancunier(ej);   
    int tabTest10[] = {
        1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1,/*adversaire*/
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, /*joueur*/
        
        0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, /*adversaire*/
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /*joueur*/
    };
    testData(tabTest10, nbTest, nbConfrontationParTest, ej, mj, tabGain);
}