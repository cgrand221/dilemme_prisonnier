
#ifndef TEST_JOUEURDONNANTDONNANT_H
#define TEST_JOUEURDONNANTDONNANT_H

#include "../common/definitions.h"
#include "../joueurs/JoueurDonnantDonnant.h"
#include "commonTest.h"

void testJoueurDonnantDonnant(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURDONNANTDONNANT_H */

