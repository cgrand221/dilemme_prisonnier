

#ifndef TEST_JOUEURCCT_H
#define TEST_JOUEURCCT_H

#include "../common/definitions.h"
#include "../joueurs/JoueurCCT.h"
#include "commonTest.h"

void testJoueurCCT(etapeJeu * ej, memoireJoueur * mj, int *tabGain);

#endif /* TEST_JOUEURCCT_H */

