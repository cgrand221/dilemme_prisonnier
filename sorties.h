
#ifndef SORTIES_H
#define SORTIES_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "joueurs/declarerJoueurs.h"
#include "common/definitions.h"


void ecrireTabulation(char * buff, char carEspace, int tailleTabulation);
void afficher_resultats(int ** tabEffectifEvolution, int detailEffectifEvolution, int ** tabGainEvolution, int detailGainEvolution, int afficherGains, int nbEvolutions, int tailleTabulation, char * separateurColonne);

#endif /* SORTIES_H */

