

#ifndef MOTEURJEU_H
#define MOTEURJEU_H

#define JOUEUR_1_TRAHI 0
#define JOUEUR_1_COOPERE 1
#define JOUEUR_2_TRAHI 0
#define JOUEUR_2_COOPERE 1

#define JOUEUR_1 0
#define JOUEUR_2 1

void initMoteur(
    int *tabGain,
    int gainJoueur1_j1Trahi_j2Trahi, 
    int gainJoueur1_j1Trahi_j2Coopere, 
    int gainJoueur1_j1Coopere_j2Trahi, 
    int gainJoueur1_j1Coopere_j2Coopere, 
            
    int gainJoueur2_j1Trahi_j2Trahi, 
    int gainJoueur2_j1Trahi_j2Coopere, 
    int gainJoueur2_j1Coopere_j2Trahi, 
    int gainJoueur2_j1Coopere_j2Coopere
);


int * getGainJoueur1Joueur2(int * tabGain, int j1TrahirOuCooperer, int j2TrahirOuCooperer);

#endif /* MOTEURJEU_H */

