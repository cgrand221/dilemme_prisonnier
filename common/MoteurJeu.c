#include "MoteurJeu.h"

void initMoteur(
    int *tabGain,
    int gainJoueur1_j1Trahi_j2Trahi, 
    int gainJoueur1_j1Trahi_j2Coopere, 
    int gainJoueur1_j1Coopere_j2Trahi, 
    int gainJoueur1_j1Coopere_j2Coopere, 
            
    int gainJoueur2_j1Trahi_j2Trahi, 
    int gainJoueur2_j1Trahi_j2Coopere, 
    int gainJoueur2_j1Coopere_j2Trahi, 
    int gainJoueur2_j1Coopere_j2Coopere
){
    tabGain[JOUEUR_1_TRAHI*4 + JOUEUR_2_TRAHI*2 + JOUEUR_1] = gainJoueur1_j1Trahi_j2Trahi;
    tabGain[JOUEUR_1_TRAHI*4 + JOUEUR_2_TRAHI*2 + JOUEUR_2] = gainJoueur2_j1Trahi_j2Trahi;    
    tabGain[JOUEUR_1_TRAHI*4 + JOUEUR_2_COOPERE*2 + JOUEUR_1] = gainJoueur1_j1Trahi_j2Coopere;
    tabGain[JOUEUR_1_TRAHI*4 + JOUEUR_2_COOPERE*2 + JOUEUR_2] = gainJoueur2_j1Trahi_j2Coopere;
    
    tabGain[JOUEUR_1_COOPERE*4 + JOUEUR_2_TRAHI*2 + JOUEUR_1] = gainJoueur1_j1Coopere_j2Trahi;
    tabGain[JOUEUR_1_COOPERE*4 + JOUEUR_2_TRAHI*2 + JOUEUR_2] = gainJoueur2_j1Coopere_j2Trahi;    
    tabGain[JOUEUR_1_COOPERE*4 + JOUEUR_2_COOPERE*2 + JOUEUR_1] = gainJoueur1_j1Coopere_j2Coopere;
    tabGain[JOUEUR_1_COOPERE*4 + JOUEUR_2_COOPERE*2 + JOUEUR_2] = gainJoueur2_j1Coopere_j2Coopere;   
}


int * getGainJoueur1Joueur2(int * tabGain, int j1TrahirOuCooperer, int j2TrahirOuCooperer){
    return tabGain + j1TrahirOuCooperer*4 + j2TrahirOuCooperer*2;
}