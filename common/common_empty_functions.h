

#ifndef COMMONEMPTYFUNCTIONS_H
#define COMMONEMPTYFUNCTIONS_H

#include "definitions.h"

void initJoueurFaitRien(memoireJoueur * mj);
void setResultatConfrontationFaitRien(memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);


#endif /* COMMONEMPTYFUNCTIONS_H */

