#include "GestionEffectif.h"

int comparer_entier(const void *a, const void *b)
{
    const int *ia = (const int *)a;
    const int *ib = (const int *)b;
    return (*ia)  - (*ib); 
}


void repartirEffectif(int * tabEffectif, int nbRepartir, int nbJoueurParType){
    int i;
    int tabEffectifCumule[nbRepartir];
    
    for(i=0;i<nbRepartir-1;i++){
        tabEffectifCumule[i] = rand() % (nbJoueurParType+1);
    }
    tabEffectifCumule[nbRepartir-1]  = nbJoueurParType;
    qsort(tabEffectifCumule, nbRepartir, sizeof(int), comparer_entier);
    
    int dernierEffectif = 0;
    for(i=0;i<nbRepartir;i++){
        tabEffectif[i] = tabEffectifCumule[i] - dernierEffectif;
        dernierEffectif = tabEffectifCumule[i];
    }
    
   
}

void ajusterEffectif(int nbTypeJoueur,  int * tabEffectif, int nbJoueurParType){
    int numeroJoueur1, numeroJoueur2;
    
    int continuer;
    
    void  (*setEtapeJoueur1)(etapeJeu *);
    void  (*setEtapeJoueur2)(etapeJeu *);
    void  (**tabSetEtapeJoueur)(etapeJeu *);
    etapeJeu * ej1 = (etapeJeu *) malloc(sizeof(*ej1));
    etapeJeu * ej2 = (etapeJeu *) malloc(sizeof(*ej2));
    tabSetEtapeJoueur = (void  (**)(etapeJeu *)) malloc(sizeof(*tabSetEtapeJoueur) * nbTypeJoueur);
    
    listeEtapeJoueurs(tabSetEtapeJoueur);
    
    
    numeroJoueur1 = 0;
    while(numeroJoueur1 < nbTypeJoueur){
        setEtapeJoueur1 = tabSetEtapeJoueur[numeroJoueur1];
        setEtapeJoueur1(ej1);
        numeroJoueur2 = numeroJoueur1+1;
        continuer =  (numeroJoueur2 < nbTypeJoueur);
            
        while(continuer){ 
            setEtapeJoueur2 = tabSetEtapeJoueur[numeroJoueur2];
            setEtapeJoueur2(ej2);
            continuer = (strcmp(ej1->nomJoueur, ej2->nomJoueur) == 0);
            
            if(continuer){
                numeroJoueur2++;
                continuer = (numeroJoueur2 < nbTypeJoueur);
            }
        }        
        
        repartirEffectif(tabEffectif+numeroJoueur1, (numeroJoueur2-numeroJoueur1), nbJoueurParType);
        
        numeroJoueur1 = numeroJoueur2;
    }
    
    free(ej1);
    free(ej2);
    free(tabSetEtapeJoueur);
    
}