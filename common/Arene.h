

#ifndef ARENE_H
#define ARENE_H

#include <stdlib.h>
#include "definitions.h"
#include "MoteurJeu.h"
#include "../joueurs/declarerJoueurs.h"

typedef struct gainNumeroJoueur{
    int gain;
    int numeroJoueur;
} gainNumeroJoueur;


int comparer_gain(const void *a, const void *b);
void faireDupliquer(int nbTypeJoueur, int seuilDulication, int * tabEffectif, gainNumeroJoueur ** tabGainJoueursTriJoueur);
void faireMourir(int nbTypeJoueur, int seuilMort, int * tabEffectif, gainNumeroJoueur ** tabGainJoueursTriJoueur);
void confrontationEntreJoueurs(
        int * tabGain, 
        void  (**tabSetEtapeJoueur)(etapeJeu *), 
        int nbTypeJoueur,
        etapeJeu * ej1, 
        etapeJeu * ej2, 
        memoireJoueur * mj1, 
        memoireJoueur * mj2, 
        int nbConfrontationAvantEvolution,
        gainNumeroJoueur ** tabRetourGainJoueurs,
        int utiliserCache1,
        int * tabEffectif,
        int ** cacheGainIndividuels
);
void arene(
        int * tabGain, 
        int nbTypeJoueur, 
        int ** tabEffectifEvolution, 
        int detailEffectifEvolution, 
        int ** tabGainEvolution, 
        int detailGainEvolution, 
        int nbConfrontationAvantEvolution, 
        int nbGenerations, 
        int seuilMort, 
        int seuilDulication
);

#endif /* ARENE_H */

