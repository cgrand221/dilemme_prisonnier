
#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define TRAHIR 0
#define COOPERER 1

typedef struct memoireJoueur{
    int dernierCoupAdversaire;
    int nbTrahirProchainsCoups;
    int nbTrahisonsTotalAdversaire;
    int nbTrahisonMoiEnCours;
    int nbCooperationsTotalAdversaire;
    int gainMoiAuDernierCoup;
    int jeuMoiAuDernierCoup;
    int nbConfrontation;
    int etatConfrontation;
} memoireJoueur;

typedef struct etapeJeu{
    void (*init)(memoireJoueur * mj);
    int (*jouerJoueur)(memoireJoueur * mj);
    void (*setResultatConfrontation) (memoireJoueur * mj, int monCoup,int coupAdversaire, int monGain, int gainAdversaire);
    const char * nomJoueur;
    int estAleatoire;
} etapeJeu;


#endif /* DEFINITIONS_H */

