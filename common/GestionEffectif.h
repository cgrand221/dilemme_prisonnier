

#ifndef GESTION_EFFECTIF_H
#define GESTION_EFFECTIF_H

#include <stdlib.h>
#include <string.h>
#include "definitions.h"
#include "../joueurs/declarerJoueurs.h"

int comparer_entier(const void *a, const void *b);
void repartirEffectif(int * tabEffectif, int nbRepartir, int nbJoueurParType);
void ajusterEffectif(int nbTypeJoueur,  int * tabEffectif, int nbJoueurParType);

#endif /* GESTION_EFFECTIF_H */

