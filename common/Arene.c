#include "Arene.h"

#define USECHACHE1 1
#define USECHACHE2 1

int comparer_gain(const void *a, const void *b)
{
    const gainNumeroJoueur **ia = (const gainNumeroJoueur **)a;
    const gainNumeroJoueur **ib = (const gainNumeroJoueur **)b;
    /*if((*ia)->gain == (*ib)->gain){
        return ((rand()%2) == 1 ? 1 : -1) / *(*ia)->numeroJoueur  - (*ib)->numeroJoueur* /;  
    }*/
    return (*ia)->gain  - (*ib)->gain; 
}



void faireDupliquer(int nbTypeJoueur, int seuilDulication, int * tabEffectif, gainNumeroJoueur ** tabGainJoueursTriJoueur){
    int effectifDuplique = 0;
    int i = nbTypeJoueur-1;
    int numeroJoueur;
    
    while((i >= 0) && (effectifDuplique < seuilDulication)){
        numeroJoueur = tabGainJoueursTriJoueur[i]->numeroJoueur;/*type de joueur le plus fort*/
        effectifDuplique += tabEffectif[numeroJoueur];
        tabEffectif[numeroJoueur] = 2*tabEffectif[numeroJoueur];
        i--;
    }
    if(effectifDuplique > seuilDulication){
        tabEffectif[numeroJoueur] -= (effectifDuplique-seuilDulication);
        /*      
            effectifDuplique -= (effectifDuplique-seuilDulication);
        <=> effectifDuplique = effectifDuplique - (effectifDuplique-seuilDulication);
        <=> effectifDuplique = seuilDulication;
        */
    }
}
void faireMourir(int nbTypeJoueur, int seuilMort, int * tabEffectif, gainNumeroJoueur ** tabGainJoueursTriJoueur){
    int effectifMort = 0;
    int i = 0;
    int numeroJoueur;
    
    while((i < nbTypeJoueur) && (effectifMort < seuilMort)){
        numeroJoueur = tabGainJoueursTriJoueur[i]->numeroJoueur;/*type de joueur le plus faible*/
        effectifMort += tabEffectif[numeroJoueur];
        tabEffectif[numeroJoueur] = 0;
        i++;
    }
    if(effectifMort > seuilMort){
        tabEffectif[numeroJoueur] = effectifMort-seuilMort;
        /*      
            effectifMort -= (effectifMort-seuilMort);
        <=> effectifMort = effectifMort - (effectifMort-seuilMort);
        <=> effectifMort = seuilMort;
        */
    }
}


void confrontationEntreJoueurs(
        int * tabGain, 
        void  (**tabSetEtapeJoueur)(etapeJeu *), 
        int nbTypeJoueur,
        etapeJeu * ej1, 
        etapeJeu * ej2, 
        memoireJoueur * mj1, 
        memoireJoueur * mj2, 
        int nbConfrontationAvantEvolution,
        gainNumeroJoueur ** tabRetourGainJoueurs,
        int utiliserCache1,
        int * tabEffectif,
        int ** cacheGainIndividuels
){
    int  k, l, coup1, coup2, gain1, gain2, numeroJoueur1, numeroJoueur2, effectif, joueursAleatoire, nbInterraction;
   
    int * gainJoueurs;
    int gainTmp1;
    int gainTmp2;
    
    void  (*setEtapeJoueur1)(etapeJeu *);
    void  (*setEtapeJoueur2)(etapeJeu *);
    
    for(numeroJoueur1 = 0;numeroJoueur1 < nbTypeJoueur;numeroJoueur1++){
        setEtapeJoueur1 = tabSetEtapeJoueur[numeroJoueur1];
        setEtapeJoueur1(ej1);
        for(numeroJoueur2 = numeroJoueur1;numeroJoueur2 < nbTypeJoueur;numeroJoueur2++){/*tous les type de joueurs doivent se confronter a eux meme*/
            setEtapeJoueur2 = tabSetEtapeJoueur[numeroJoueur2];
            setEtapeJoueur2(ej2);
            
            joueursAleatoire = (!USECHACHE2 || ej1->estAleatoire || ej2->estAleatoire);
            
            
            if(numeroJoueur1 == numeroJoueur2){
                /*
                Pour n joueurs du meme type :
                Le 1er interragie n-1 avec les autres 
                Le 2e  interragie n-2 avec les autres 
                ...
                le dernier n'interagie avec personne

                (*) Soit (n-1)+(n-2)+...+1+0=(n-1)n/2 
                */
                nbInterraction = (tabEffectif[numeroJoueur1]-1) * tabEffectif[numeroJoueur1] / 2;
            }
            else{
                /*
                 Soit n joueurs de type 1 (le type est choisit au depart)
                 Soit m joueurs de type 2 (le type est choisit au depart)

                 le 1er joueur de type 1 interragie avec le 1er joueur de type 2
                 le 1er joueur de type 1 interragie avec le 2e  joueur de type 2
                 ...
                 le 1er joueur de type 1 interragie avec le me  joueur de type 2
                 le 2e  joueur de type 1 interragie avec le 1er joueur de type 2
                 le 2e  joueur de type 1 interragie avec le 2e  joueur de type 2
                 ...
                 le ne  joueur de type 1 interragie avec le 2e  joueur de type 2

                 Soit nm fois (**)

                 De plus si on simule les interractions entre 2 types de joueurs différents d'effectifs n et m :

                 le 1er type fera (n-1)n/2 interactions avec lui meme (en utilisant (*))
                 le 2e  type aura (m-1)m/2 interactions avec lui meme (en utilisant (*))

                 le 1er type fera nm interactions avec le 2e  type (en utilisant (**))

                 Si on récapitule, le nombre d'inraction totales est
                 (n-1)n/2 + (m-1)m/2 + mn   = (1/2) ( (n-1)n + (m-1)m + mn + mn) 
                                            = (1/2) ( (n+m-1)n + (n+m-1)m ) 
                                            = (n+m-1) * (n+m) / 2 

                 Avec (*) le nombre d'inraction totales est aussi (n+m-1) * (n+m) / 2 
                 */
                nbInterraction = tabEffectif[numeroJoueur1] * tabEffectif[numeroJoueur2];
            }
            
            
            if(!utiliserCache1 || joueursAleatoire){
                
                if(joueursAleatoire){
                    effectif = nbInterraction;
                }
                else{
                    effectif = 1;
                }
                gain1 = 0;
                gain2 = 0;
                
                for(l=0;l<effectif;l++){
                    ej1->init(mj1);
                    ej2->init(mj2);  
                    for(k = 0;k < nbConfrontationAvantEvolution;k++){
                        coup1 = ej1->jouerJoueur(mj1);
                        coup2 = ej2->jouerJoueur(mj2);

                        gainJoueurs = getGainJoueur1Joueur2(tabGain, coup1, coup2);
                        gainTmp1 = *gainJoueurs;
                        gainTmp2 = *(gainJoueurs+1);

                        ej1->setResultatConfrontation(mj1, coup1, coup2, gainTmp1, gainTmp2);
                        ej2->setResultatConfrontation(mj2, coup2, coup1, gainTmp2, gainTmp1);

                        gain1 += gainTmp1;
                        gain2 += gainTmp2;
                        
                       

                    }
                }
                if(!joueursAleatoire){
                    cacheGainIndividuels[numeroJoueur1][numeroJoueur2] = gain1;
                    cacheGainIndividuels[numeroJoueur2][numeroJoueur1] = gain2;
                }
            }
            if(!joueursAleatoire){
                if(numeroJoueur1 == numeroJoueur2){
                    gain1 = cacheGainIndividuels[numeroJoueur1][numeroJoueur2] * nbInterraction;
                    gain2 = gain1;
                }
                else{
                    gain1 = cacheGainIndividuels[numeroJoueur1][numeroJoueur2] * nbInterraction;
                    gain2 = cacheGainIndividuels[numeroJoueur2][numeroJoueur1] * nbInterraction;
                }
            }
           
            
            
            tabRetourGainJoueurs[numeroJoueur1]->gain += gain1;
            tabRetourGainJoueurs[numeroJoueur2]->gain += gain2;
        }
    }
    for(numeroJoueur1 = 0;numeroJoueur1 < nbTypeJoueur;numeroJoueur1++){
        if( tabEffectif[numeroJoueur1] > 0){
            tabRetourGainJoueurs[numeroJoueur1]->gain = tabRetourGainJoueurs[numeroJoueur1]->gain / tabEffectif[numeroJoueur1];
        }
        
    }
}




void arene(
        int * tabGain, 
        int nbTypeJoueur, 
        int ** tabEffectifEvolution, 
        int detailEffectifEvolution, 
        int ** tabGainEvolution, 
        int detailGainEvolution, 
        int nbConfrontationAvantEvolution, 
        int nbGenerations, 
        int seuilMort, 
        int seuilDulication
){
    
    int tempsEvolution;
    int i;
    
    
    etapeJeu * ej1 = (etapeJeu *) malloc(sizeof(*ej1));
    etapeJeu * ej2 = (etapeJeu *) malloc(sizeof(*ej2));
    
    memoireJoueur * mj1 = (memoireJoueur *) malloc (sizeof(*mj1));
    memoireJoueur * mj2 = (memoireJoueur *) malloc (sizeof(*mj2));
    
    void  (**tabSetEtapeJoueur)(etapeJeu *);
    tabSetEtapeJoueur = (void  (**)(etapeJeu *)) malloc(sizeof(*tabSetEtapeJoueur) * nbTypeJoueur);
    
    listeEtapeJoueurs(tabSetEtapeJoueur);
     
   
    

    gainNumeroJoueur ** tabGainJoueursTriJoueur = (gainNumeroJoueur **) malloc(sizeof(gainNumeroJoueur *) * nbTypeJoueur);    
    gainNumeroJoueur ** tabGainJoueursTriGain = (gainNumeroJoueur **) malloc(sizeof(gainNumeroJoueur *) * nbTypeJoueur);
    int ** cacheGainIndividuels = (int **)malloc(sizeof(int *) * nbTypeJoueur);
    for(i = 0;i < nbTypeJoueur;i++){
        tabGainJoueursTriJoueur[i] = (gainNumeroJoueur *) malloc(sizeof(gainNumeroJoueur) * 2);
        cacheGainIndividuels[i] = (int *) malloc (sizeof(int) * nbTypeJoueur);
    }
    
    int iterationEvolutionSuivant;
   
    
    int iterationEvolution = 0;
    for (tempsEvolution = 1;tempsEvolution < nbGenerations;tempsEvolution++){
        
        for(i = 0;i < nbTypeJoueur;i++){
            tabGainJoueursTriJoueur[i]->gain = 0;
            tabGainJoueursTriJoueur[i]->numeroJoueur = i;
        }
        

        confrontationEntreJoueurs(
            tabGain, 
            tabSetEtapeJoueur, 
            nbTypeJoueur,
            ej1, 
            ej2, 
            mj1, 
            mj2, 
           
            nbConfrontationAvantEvolution,
            tabGainJoueursTriJoueur,
            (tempsEvolution > 1) && USECHACHE1,
            tabEffectifEvolution[iterationEvolution],
            cacheGainIndividuels
        );
        
        if(detailGainEvolution){
            for(i = 0;i < nbTypeJoueur;i++){
                tabGainEvolution[tempsEvolution][i] = tabGainJoueursTriJoueur[i]->gain;
            }            
        }
        
        for(i = 0;i < nbTypeJoueur;i++){
            tabGainJoueursTriGain[i] = tabGainJoueursTriJoueur[i];
        }        
        qsort(tabGainJoueursTriGain, nbTypeJoueur, sizeof(gainNumeroJoueur*), comparer_gain);
        
        if(detailEffectifEvolution == 1){
            iterationEvolutionSuivant = iterationEvolution+1;
        }
        else{
            iterationEvolutionSuivant = 1-iterationEvolution;
        }
        
        for(i = 0;i < nbTypeJoueur;i++){//on fait une copie
            tabEffectifEvolution[iterationEvolutionSuivant][i] = tabEffectifEvolution[iterationEvolution][i];
        }
       
        iterationEvolution = iterationEvolutionSuivant;
        
        // on travaille sur la copie
        faireMourir(nbTypeJoueur, seuilMort, tabEffectifEvolution[iterationEvolutionSuivant], tabGainJoueursTriGain);
        faireDupliquer(nbTypeJoueur, seuilDulication, tabEffectifEvolution[iterationEvolutionSuivant], tabGainJoueursTriGain);
        
        
    }
    if(!detailGainEvolution){
        for(i = 0;i < nbTypeJoueur;i++){
            tabGainEvolution[0][i] = tabGainJoueursTriJoueur[i]->gain;
        }            
    }
    
    
    for(i = 0;i < nbTypeJoueur;i++){
        free(tabGainJoueursTriJoueur[i]);
        free(cacheGainIndividuels[i]);
    }
    free(tabGainJoueursTriJoueur);
    free(cacheGainIndividuels);
    free(tabGainJoueursTriGain);
    
    
    
    free(ej1);
    free(ej2);
    free(mj1);
    free(mj2);
    free(tabSetEtapeJoueur);
}

