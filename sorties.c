#include "sorties.h"

void ecrireTabulation(char * buff, char carEspace, int tailleTabulation){
    int taille = strlen(buff);
    int nbEspace = tailleTabulation - (taille%tailleTabulation);
    buff+=taille;
    while(nbEspace > 0){
        *buff = carEspace;
        nbEspace--;
        buff++;
    }
    *buff='\0';
}


void afficherTableau(char * separateurColonne, int tailleTabulation, int ** tabEffectifGain, int depart, int arrivee, int generation){
    int i,j,total;
    
    size_t sizeBuff = sizeof(char) * TAILLEBUFF;
    char * buff = (char *) malloc (sizeBuff);
    char * nbre = (char *) malloc (sizeBuff);
    
    
    buff[0] = '\0';
    ecrireTabulation(buff, ' ', tailleTabulation);
    strncat(buff, separateurColonne, sizeBuff);
    ecrireTabulation(buff, ' ', tailleTabulation);
    strncat(buff, "Joueurs", sizeBuff);
    printf("%s\n", buff);
    
    
    buff[0] = '\0';
    ecrireTabulation(buff, '-', tailleTabulation);
    strncat(buff, separateurColonne, sizeBuff);
    ecrireTabulation(buff, '-', tailleTabulation*(NBTYPEJOUEUR+3)); 
    printf("%s\n", buff);
    
    
    
    buff[0] = '\0';
    strncat(buff, "gen", sizeBuff);
    ecrireTabulation(buff, ' ', tailleTabulation);
    strncat(buff, separateurColonne, sizeBuff);
    for(j = 0;j < NBTYPEJOUEUR;j++){
        ecrireTabulation(buff, ' ', tailleTabulation);
        snprintf(nbre, sizeBuff, "%d", j);
        strncat(buff, nbre, sizeBuff);
    }
    ecrireTabulation(buff, ' ', tailleTabulation);
    strncat(buff, "total", sizeBuff);
    printf("%s\n", buff);
    
    
    for(i = depart;i < arrivee;i++){
        buff[0] = '\0';
        snprintf(nbre, sizeBuff, "%d", generation);
        strncat(buff, nbre, sizeBuff);
        ecrireTabulation(buff, ' ', tailleTabulation);
        strncat(buff, separateurColonne, sizeBuff);
        total = 0;
        for(j = 0;j < NBTYPEJOUEUR;j++){
            ecrireTabulation(buff, ' ', tailleTabulation);
            snprintf(nbre, sizeBuff, "%d", tabEffectifGain[i][j]);
            strncat(buff, nbre, sizeBuff);
            total += tabEffectifGain[i][j];
        }
        ecrireTabulation(buff, ' ', tailleTabulation);
        snprintf(nbre, sizeBuff, "%d", total);
        strncat(buff, nbre, sizeBuff);
        printf("%s\n", buff);
        
        
        generation++;
    }
    free(buff);
    free(nbre);
}

void afficher_resultats(int ** tabEffectifEvolution, int detailEffectifEvolution, int ** tabGainEvolution, int detailGainEvolution, int afficherGains, int nbEvolutions, int tailleTabulation, char * separateurColonne){
    
    int i;
    void  (*tabSetEtapeJoueur[NBTYPEJOUEUR])(etapeJeu *);
    void  (*setEtapeJoueur)(etapeJeu *);
    etapeJeu * ej = (etapeJeu *) malloc(sizeof(*ej));
    
    listeEtapeJoueurs(tabSetEtapeJoueur);
    
    printf("Legende: NomTypeJoueur: n°\n");
    for (i = 0;i < NBTYPEJOUEUR;i++){
        setEtapeJoueur = tabSetEtapeJoueur[i];
        setEtapeJoueur(ej);
        printf("%s: %d\n", ej->nomJoueur, i);
    }    
    printf("\n");
    
    
    printf("Effectifs:\n");
    int depart = 0;
    int arrivee = nbEvolutions;
    int generation = 0;
    if(detailEffectifEvolution == 0){
        generation = (nbEvolutions-1);
        depart = generation%2;
        arrivee = depart+1;        
    }        
    afficherTableau(separateurColonne, tailleTabulation, tabEffectifEvolution, depart, arrivee, generation);
    
    
    if(afficherGains){
        printf("\n");
        printf("Gains:\n");
        depart = 0;
        arrivee = nbEvolutions;
        generation = 0;
        if(detailGainEvolution == 0){
            generation = (nbEvolutions-1);
            depart = 0;
            arrivee = 1;   
        }
        afficherTableau(separateurColonne, tailleTabulation, tabGainEvolution, depart, arrivee, generation);
    }
   
    
    free(ej);
}